Add Mifare 1k support for Proxmark
Add manual card entry support for HID cards
Add detailed view for HID card data
Allow manual editing of HID card data
Decoding of HID card data
Better device support for Proxmark including RDV4
